let letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k']
describe('Los estudiantes under monkeys', function () {
    it('visits los estudiantes and survives monkeys', function () {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
        cy.wait(1000);
        randomEvent(10);
    });
});


function randomEvent(monkeysLeft) {
    function onFinish(resp) {
        if (resp)
            setTimeout(randomEvent, 2000, monkeysLeft - 1);
        else
            randomEvent();
    }
    if (monkeysLeft) {
        var i = getRandomInt(0, 4);
        if (i === 0) {
            randomClick('a').then(onFinish);
        } else if (i === 1) {
            randomFill().then(onFinish);
        } else if (i === 2) {
            randomSelect().then(onFinish);
        } else if (i === 3) {
            randomClick('button').then(onFinish);
        }
    }

}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

function randomChar() {
    var n = getRandomInt(32, 128)
    return String.fromCharCode(n);
}

function randomFill() {
    return getRandomElementWithType('input').then(randomInput => {
        if (randomInput && !Cypress.Dom.isHidden(randomInput)) {
            console.log("TEXT", randomInput[0].type === "text");
            if (randomInput[0].type === "text") {
                cy.wrap(randomInput).click({
                    force: true
                });
                return cy.wrap(randomInput).type(randomChar()).then(() => true);
            } else {
                return false;
            }
        } else {
            return false;
        }

    });
}

function randomSelect() {
    return getRandomElementWithType('select').then(select => {
        if (select && !Cypress.Dom.isHidden(select)) {
            var options = select[0].options;
            var randomOption = options[getRandomInt(0, options.length)];
            if (!randomOption.disabled) {
                return cy.wrap(select).select(randomOption.value).then(() => true);
            } else {
                return false;
            }
        } else {
            return false;
        }

    });
}

function randomClick(type) {
    return getRandomElementWithType(type).then(randomLink => {
        console.log("randomLink", randomLink)
        if (Cypress.Dom.isHidden(randomLink)) {
            return randomClick();
        } else {
            return cy.wrap(randomLink).click({
                force: true
            }).then(() => true);
        }
    });
}

function getRandomElementWithType(type) {
    return cy.get(type).then($elems => {
        return $elems.get(getRandomInt(0, $elems.length));
    });
}