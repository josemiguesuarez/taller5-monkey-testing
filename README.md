**Taller 5**

1 . Cypress-monkey
Se creó la función randomEvent() en la que se llama a las funciones randomFill, randomSelect y randomClick. randomClick recibe como parámetro los elementos en los que debería hacer click que pueden ser botones o enlaces. 

[ver código](https://gitlab.com/josemiguesuarez/taller5-monkey-testing/blob/master/cypress-monkey/cypress/integration/monkey_testing_ripper.spec.js)

2 . Gremlins-webdriver
Se configuró la horda como se especifica en el taller

[ver código](https://gitlab.com/josemiguesuarez/taller5-monkey-testing/blob/master/gremlins-webdriver/test/specs/gremlins-test.js)

3 . Android Monkey Testing

Se descargó Wikipedia y se realizó la prueba con el siguiente comando en MacOS:

adb shell monkey -p org.wikipedia -v 10000

[enlace al GIF](/uploads/e41eb77691573dcd2190029b47069c4f/output.gif)
![output](/uploads/e41eb77691573dcd2190029b47069c4f/output.gif)